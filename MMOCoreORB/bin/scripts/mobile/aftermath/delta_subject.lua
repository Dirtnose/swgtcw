delta_subject = Creature:new {
	customName = "Project Delta Subject",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 278,
	chanceHit = 27.25,
	damageMin = 1520,
	damageMax = 2750,
	baseXp = 26654,
	baseHAM = 321000,
	baseHAMmax = 392000,
	armor = 2,
	resists = {70,70,70,70,70,70,70,70,70},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_boss_m_ith_01.iff",
			"object/mobile/outbreak_undead_boss_m_rod_01.iff",
			"object/mobile/outbreak_undead_boss_m_sul_01.iff",
			"object/mobile/outbreak_undead_boss_m_trn_01.iff",
			"object/mobile/outbreak_undead_boss_m_twe_01.iff",
			"object/mobile/outbreak_undead_boss_m_wke_01.iff",
			"object/mobile/outbreak_undead_boss_m_wke_02.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 1500000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 2000000
		}
	},
	weapons = {""},
	conversationTemplate = "",
	attacks = merge(tkamaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(delta_subject, "delta_subject")
