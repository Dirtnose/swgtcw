event_cultist_sorcerer_b = Creature:new {
	customName = "Cultist Sorcerer",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "dark_jedi",
	faction = "",
	level = 150,
	chanceHit = 30,
	damageMin = 350,
	damageMax = 850,
	baseXp = 45,
	baseHAM = 375000,
	baseHAMmax = 425000,
	armor = 2,
	resists = {60,60,65,65,50,65,70,90,65},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/exar_kun_cultist.iff",
		},
	lootGroups = {
		{
			groups = {
				{group = "krayt_pearls", chance = 5000000},
				{group = "named_color_crystals", chance = 5000000},
				{group = "event_token", chance = 5000000}

			},
			lootChance = 15000000
		},
		{
			groups = {
				{group = "krayt_pearls", chance = 5000000},
				{group = "named_color_crystals", chance = 5000000},
				{group = "event_token", chance = 5000000}

			},
			lootChance = 15000000
		}
	},
	weapons = {"dark_jedi_weapons_gen3"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(event_cultist_sorcerer_b, "event_cultist_sorcerer_b")
