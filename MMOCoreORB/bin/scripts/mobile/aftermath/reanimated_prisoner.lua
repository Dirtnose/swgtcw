reanimated_prisoner = Creature:new {
	customName = "Reanimated Prisoner",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 37,
	chanceHit = 0.6,
	damageMin = 460,
	damageMax = 800,
	baseXp = 2730,
	baseHAM = 17200,
	baseHAMmax = 18800,
	armor = 1,
	resists = {50,50,50,50,50,50,50,50,50},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_prisoner_m_zab_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_04.iff",
			"object/mobile/outbreak_undead_prisoner_m_bith_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_bith_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_hum_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_hum_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_hum_03.iff",
			"object/mobile/outbreak_undead_prisoner_m_hum_04.iff",
			"object/mobile/outbreak_undead_prisoner_m_hum_05.iff",
			"object/mobile/outbreak_undead_prisoner_m_ith_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_ith_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_monc_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_monc_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_twi_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_twi_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_wke_01.iff",
			"object/mobile/outbreak_undead_prisoner_m_wke_02.iff",
			"object/mobile/outbreak_undead_prisoner_m_zab_02.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 1000000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 1000000
		}
	},
	weapons = {""},
	conversationTemplate = "",
	attacks = merge(tkamaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(reanimated_prisoner, "reanimated_prisoner")
