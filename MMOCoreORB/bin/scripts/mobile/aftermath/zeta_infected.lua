zeta_infected = Creature:new {
	customName = "Project Zeta Subject",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 278,
	chanceHit = 27.25,
	damageMin = 1520,
	damageMax = 2750,
	baseXp = 26654,
	baseHAM = 321000,
	baseHAMmax = 392000,
	armor = 2,
	resists = {70,70,70,70,70,70,70,70,70},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_boss_f_both_01.iff",
			"object/mobile/outbreak_undead_boss_f_hum_01.iff",
			"object/mobile/outbreak_undead_boss_f_hum_02.iff",
			"object/mobile/outbreak_undead_boss_f_ith_01.iff",
			"object/mobile/outbreak_undead_boss_m_hum_01.iff",
			"object/mobile/outbreak_undead_boss_m_hum_02.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 2500000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 2000000
		}
	},
	weapons = {""},
	conversationTemplate = "",
	attacks = merge(tkamaster,brawlermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(zeta_infected, "zeta_infected")
                    