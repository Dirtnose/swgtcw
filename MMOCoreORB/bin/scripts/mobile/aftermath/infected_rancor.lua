infected_rancor = Creature:new {
	customName = "Infected Rancor",
	socialGroup = "nightsister",
	faction = "",
	level = 336,
	chanceHit = 30.0,
	damageMin = 2270,
	damageMax = 4250,
	baseXp = 28549,
	baseHAM = 310000,
	baseHAMmax = 401000,
	armor = 3,
	resists = {80,80,80,80,80,80,80,80,40},
	meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_bristley",
	hideAmount = 950,
	boneType = "bone_mammal",
	boneAmount = 905,
	milk = 0,
	tamingChance = 0,
	ferocity = 30,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,
	scale = 2,

	templates = {"object/mobile/wod_mutant_rancor_boss.iff"},
	hues = {},
	lootGroups = {
	    {
	    groups = {
			    {group = "halloween_token", chance = 10000000}
			            },
	        lootChance = 10000000
		},
		{
	    groups = {
				{group = "halloween_token", chance = 10000000}
			},
			lootChance = 10000000
			},
		{
	    groups = {
				{group = "halloween_token", chance = 10000000}
			},
			lootChance = 10000000
			},
		{
	    groups = {
				{group = "halloween_token", chance = 10000000}
			},
			lootChance = 10000000
			}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"creatureareacombo","stateAccuracyBonus=100"},
		{"creatureareaknockdown","stateAccuracyBonus=100"},
		{"creatureareadisease",""}
	}
}

CreatureTemplates:addCreatureTemplate(infected_rancor, "infected_rancor")
