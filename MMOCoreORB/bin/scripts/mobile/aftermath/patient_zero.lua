patient_zero = Creature:new {
	customName = "Patient Zero",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 278,
	chanceHit = 27.25,
	damageMin = 1520,
	damageMax = 2750,
	baseXp = 26654,
	baseHAM = 521000,
	baseHAMmax = 592000,
	armor = 3,
	resists = {70,70,70,70,70,70,70,70,70},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/outbreak_undead_boss_m_hum_04.iff"},
	lootGroups = {
		{
			groups = {
			{group = "halloween_token", chance = 10000000},
			},
			lootChance = 10000000
		    },
		{
	        groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 10000000
			},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 10000000
		}
	},
	weapons = {"ns_elder_weapons"},
	conversationTemplate = "",
	attacks = merge(tkamaster,swordsmanmaster,fencermaster,pikemanmaster,brawlermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(patient_zero, "patient_zero")
