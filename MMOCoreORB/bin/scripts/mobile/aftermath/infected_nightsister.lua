infected_nightsister = Creature:new {
	customName = "Infected Nightsister",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 278,
	chanceHit = 27.25,
	damageMin = 1520,
	damageMax = 2750,
	baseXp = 26654,
	baseHAM = 221000,
	baseHAMmax = 292000,
	armor = 2,
	resists = {70,70,70,70,70,70,70,70,70},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + HEALER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_boss_f_nightsister_01.iff",
			"object/mobile/outbreak_undead_boss_f_nightsister_02.iff",
			"object/mobile/outbreak_undead_boss_f_nightsister_03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 4000000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 2000000
		}
	},
	weapons = {"wod_weapons"},
	conversationTemplate = "",
	attacks = merge(tkamaster,swordsmanmaster,fencermaster,pikemanmaster,brawlermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(infected_nightsister, "infected_nightsister")
