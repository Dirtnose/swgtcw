event_cultist_alchemist_b = Creature:new {
	customName = "The Alchemist",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "dark_jedi",
	faction = "",
	level = 333,
	chanceHit = 30,
	damageMin = 625,
	damageMax = 1325,
	baseXp = 45,
    baseHAM = 600000,
    baseHAMmax = 750000,
	armor = 3,
	resists = {90,90,65,65,50,65,70,90,65},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/exar_kun_cultist.iff",
		},
	lootGroups = {
		{
			groups = {
				{group = "named_color_crystals", chance = 5000000},
				{group = "event_token", chance = 5000000}
			},
			lootChance = 10000000
		},
		{
			groups = {
				{group = "named_color_crystals", chance = 5000000},
				{group = "event_token", chance = 5000000}
			},
			lootChance = 10000000
		},
		{
			groups = {
				{group = "named_color_crystals", chance = 5000000},
				{group = "event_token", chance = 5000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {"dark_jedi_weapons_gen4"},
	conversationTemplate = "",
	attacks = merge(lightsabermaster,brawlermaster,forcepowermaster)
}

CreatureTemplates:addCreatureTemplate(event_cultist_alchemist_b, "event_cultist_alchemist_b")
