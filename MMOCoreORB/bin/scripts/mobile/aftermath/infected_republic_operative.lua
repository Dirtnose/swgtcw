infected_republic_operative = Creature:new {
	customName = "Infected Republic Operative",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "",
	level = 88,
	chanceHit = 0.8,
	damageMin = 745,
	damageMax = 1000,
	baseXp = 8408,
	baseHAM = 41000,
	baseHAMmax = 56000,
	armor = 2,
	resists = {60,60,60,60,60,60,60,60,60},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_scientist_f_hum_01.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_02.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_03.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_04.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_05.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_06.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_07.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_08.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_09.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_10.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_11.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_12.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_13.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_14.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_15.iff",
			"object/mobile/outbreak_undead_scientist_f_hum_16.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_01.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_02.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_03.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_04.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_05.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_06.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_07.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_08.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_09.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_10.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_11.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_12.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_13.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_14.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_15.iff",
			"object/mobile/outbreak_undead_scientist_m_hum_16.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 1000000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 2000000
		}
	},
	weapons = {"wod_weapons"},
	conversationTemplate = "",
	attacks = merge(fencermid,swordsmanmid,tkamaster,pikemanmaster,brawlermaster)
}

CreatureTemplates:addCreatureTemplate(infected_republic_operative, "infected_republic_operative")

