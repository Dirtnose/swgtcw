event_portent = Creature:new {
	customName = "Portent of Woe",
	socialGroup = "dark_jedi",
	faction = "",
	level = 157,
	chanceHit = 30,
	damageMin = 1250,
	damageMax = 2750,
	baseXp = 45,
	baseHAM = 1000000,
	baseHAMmax = 1200000,
	armor = 3,
	resists = {190,165,175,175,30,150,150,150,75},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/mutant_acklay.iff",
		},
	lootGroups = {
		{
			groups = {
				{group = "acklay", chance = 10000000}
			},
			lootChance = 10000000

		},
		{
			groups = {
				{group = "acklay", chance = 10000000}
			},
			lootChance = 10000000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack","stateAccuracyBonus=50"},
		{"creatureareacombo","stateAccuracyBonus=50"}
	}
}

CreatureTemplates:addCreatureTemplate(event_portent, "event_portent")
