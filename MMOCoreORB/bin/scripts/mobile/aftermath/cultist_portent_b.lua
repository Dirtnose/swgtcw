event_portent_b = Creature:new {
	customName = "Portent of Woe",
	socialGroup = "dark_jedi",
	faction = "",
	level = 135,
	chanceHit = 30,
	damageMin = 625,
	damageMax = 1200,
	baseXp = 45,
	baseHAM = 500000,
	baseHAMmax = 600000,
	armor = 3,
	resists = {190,165,175,175,30,150,150,150,75},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE + ENEMY,
	creatureBitmask = PACK + HERD + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/mutant_acklay.iff",
		},
	lootGroups = {
		{
			groups = {
				{group = "acklay", chance = 5000000},
				{group = "event_token", chance = 5000000}

			},
			lootChance = 10000000

		},
		{
			groups = {
				{group = "acklay", chance = 5000000},
				{group = "event_token", chance = 5000000}

			},
			lootChance = 10000000
		}
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack","stateAccuracyBonus=50"},
		{"creatureareacombo","stateAccuracyBonus=50"}
	}
}

CreatureTemplates:addCreatureTemplate(event_portent_b, "event_portent_b")
