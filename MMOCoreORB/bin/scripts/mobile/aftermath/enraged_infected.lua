enraged_infected = Creature:new {
	customName = "Enraged Infected",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "nightsister",
	faction = "nightsister",
	level = 131,
	chanceHit = 4.75,
	damageMin = 770,
	damageMax = 1250,
	baseXp = 12424,
	baseHAM = 50000,
	baseHAMmax = 61000,
	armor = 1,
	resists = {70,70,70,70,70,70,70,70,70},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
			"object/mobile/outbreak_undead_boss_m_hum_01.iff",
			"object/mobile/outbreak_undead_boss_m_hum_02.iff",
			"object/mobile/outbreak_undead_boss_m_hum_03.iff"},
	lootGroups = {
		{
			groups = {
				{group = "halloween_token", chance = 10000000},
			},
			lootChance = 2500000
		},
		{
	        groups = {
				{group = "event_token", chance = 10000000},
			},
			lootChance = 2000000
		}
	},
	weapons = {"wod_weapons"},
	conversationTemplate = "",
	attacks = merge(tkamid,brawlermaster,forcewielder)
}

CreatureTemplates:addCreatureTemplate(enraged_infected, "enraged_infected")
