jacket_gmf_01 = {
 -- Halloween Jacket Item
    minimumLevel = 0,
    maximumLevel = -1,
    customObjectName = "",
    directObjectTemplate = "object/tangible/wearables/jacket/jacket_gmf_01.iff",
    craftingValues = {},
    skillMods = {},
    customizationStringNames = {},
    customizationValues = {}
    --junkDealerTypeNeeded = JUNKGENERIC,
	--junkDealerTypeNeeded = JUNKFINERY,
    --junkMinValue = 45,
    --junkMaxValue = 90
}

addLootItemTemplate("jacket_gmf_01", jacket_gmf_01)
