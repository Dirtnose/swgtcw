/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef PYROKINESISCOMMAND_H_
#define PYROKINESISCOMMAND_H_

#include "ForcePowersQueueCommand.h"

class PyrokinesisCommand : public ForcePowersQueueCommand {
public:

	PyrokinesisCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (isWearingArmor(creature)) {
			return NOJEDIARMOR;
		}
		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target);
		CreatureObject* targetCreature = dynamic_cast<CreatureObject*>(object.get());

		if (targetCreature == NULL)
			return INVALIDTARGET;

		if (targetCreature->isPlayerCreature()){
			return INVALIDTARGET;	
		}
		
		return doCombatAction(creature, target);
	}

};

#endif //PYROKINESISCOMMAND_H_
